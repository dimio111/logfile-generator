package be.dimi.logfile.generator;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Created by Dimitri on 16/03/2015.
 */
public class NotMain {
    private static Logger LOG = LogManager.getLogger(NotMain.class);

    public static void gimmyLogging(String message){
        LOG.info(message);
    }
}
