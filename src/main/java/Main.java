import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import com.google.common.net.InetAddresses;

import java.util.Random;

/**
 * Created by Dimitri on 16/03/2015.
 */
public class Main {
    private static Logger LOG = LogManager.getLogger(Main.class);
    private static Random rn = new Random();

    public static void main(String[] args) throws InterruptedException {

        for(int i = 0 ; i < 100 ; i++){

            pickALog(rn.nextInt(9 - 1 + 1) + 1);
            Thread.sleep(rn.nextInt(2500 - 1000 + 1) + 1);
        }

    }

    public static void pickALog(Integer number){
        int amount = rn.nextInt(50 - 10 + 1) + 1;

        switch (number){
            case 1 : LOG.trace(RandomStringUtils.randomAlphabetic(amount)); break;
            case 2 : LOG.warn(RandomStringUtils.randomAlphabetic(amount)); break;
            case 3 : LOG.error(RandomStringUtils.randomAlphabetic(amount)); break;
            case 4 : LOG.debug(RandomStringUtils.randomAlphabetic(amount)); break;
            case 5 : LOG.info(RandomStringUtils.randomAlphabetic(amount)); break;
            case 6 : LOG.fatal(RandomStringUtils.randomAlphabetic(amount)); break;
            case 7 : LOG.info("customerIp " + generateRandomIp()); break;
            case 8 : LOG.info("cartId " + generatePositiveRandomInt()); break;
            case 9 : LOG.info("customerId " + generatePositiveRandomInt()); break;
            default: LOG.error(number.toString());
        }

    }

    private static String generateRandomIp(){
        return InetAddresses.fromInteger(rn.nextInt()).getHostAddress();
    }

    private static int generatePositiveRandomInt(){
        return rn.nextInt(Integer.MAX_VALUE - 1 + 1);
    }
}
